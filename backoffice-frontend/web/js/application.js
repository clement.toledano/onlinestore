$(document).ready(function () {
    $('#button').on('click', function (event) {
        event.preventDefault();
        $.get("http://localhost:8080/services/rest/work").done(function (data) {
            var catalog = '';
            data.forEach(function (nextWork) {
                catalog = catalog.concat(`${nextWork.title} (${nextWork.release}) <br/> `);
            });
            $("#button").after('<br>' + catalog);

        });
    });
});


