package com.directmedia.onlinestore.controller;

import com.directmedia.onlinestore.core.entity.Artist;
import com.directmedia.onlinestore.core.entity.Catalogue;
import com.directmedia.onlinestore.core.entity.Work;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Optional;

@WebServlet(name = "AddWorkServlet", urlPatterns = {"/add-work"})
public class AddWorkServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();
        response.setContentType("text/html");

        boolean isOk = true;


        Work nouvelleOeuvre = new Work(request.getParameter("title"));
        try {
            nouvelleOeuvre.setRelease(Integer.parseInt(request.getParameter("release")));
        } catch (NumberFormatException nfe) {
            isOk = false;
        }
        nouvelleOeuvre.setGenre(request.getParameter("genre"));
        nouvelleOeuvre.setSummary(request.getParameter("summary"));
        nouvelleOeuvre.setMainArtist(new Artist(request.getParameter("mainArtist")));

        Optional<Work> optionalWork = Catalogue.listOfWorks.stream().filter(w -> w.getTitle().equals(nouvelleOeuvre.getTitle()) && w.getRelease() == nouvelleOeuvre.getRelease() && w.getMainArtist().getName().equals(nouvelleOeuvre.getMainArtist().getName())).findAny();
        if (optionalWork.isPresent()) {
            isOk = false;
        }

        RequestDispatcher dispatcher = null;
        if (isOk) {
            Catalogue.listOfWorks.add(nouvelleOeuvre);
            request.setAttribute("id", nouvelleOeuvre.getId());

            dispatcher = request.getRequestDispatcher("/work-added-success");

        } else {
            dispatcher = request.getRequestDispatcher("/work-added-failure");
        }
        dispatcher.forward(request, response);


    }
}
