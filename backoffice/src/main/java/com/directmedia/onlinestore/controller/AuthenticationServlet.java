package com.directmedia.onlinestore.controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "AuthenticationServlet", urlPatterns = {"/login"})
public class AuthenticationServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();


        String login = request.getParameter("login");
        String password = request.getParameter("password");

        HttpSession session = request.getSession();
        session.setAttribute("login", login);


        if (login.equals("clement") && password.equals("azeqsd")) {
            out.println("votre login : " + login + " / votre password : " + password);
            out.println("Connexion accepté !");
            out.println("<br>");
            out.println("<a href=\"home.jsp\">Accés à la page home</a>");
        } else {
            out.println("login / mdp erroné !");
            out.println("<br>");
            out.println("<a href=\"login.html\">Retour à la page login</a>");
        }

    }


}
