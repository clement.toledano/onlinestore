package com.directmedia.onlinestore.controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "WorkAddedFailureServlet",urlPatterns = {"/work-added-failure"})
public class WorkAddedFailureServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out=response.getWriter();
        out.println("Une erreur est survenue, l’oeuvre n’a pas été ajoutée");
        out.print("<a href=\"home.jsp\">Retour au catalogue des oeuvres</a>");
    }


}
