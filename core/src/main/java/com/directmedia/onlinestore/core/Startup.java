package com.directmedia.onlinestore.core;

import com.directmedia.onlinestore.core.entity.Artist;
import com.directmedia.onlinestore.core.entity.Catalogue;
import com.directmedia.onlinestore.core.entity.Work;

public class Startup {
    public static void main(String[] args) {

        Artist mentoss = new Artist("Mentoss");

        Work site1 = new Work("site1");
        site1.setTitle("titre du site 1");
        site1.setGenre("genre du site 1");
        site1.setSummary("sommaire du site 1");
        site1.setRelease(2017);
        site1.setMainArtist(mentoss);

        Work site2 = new Work("site2");
        site2.setTitle("titre du site 2");
        site2.setGenre("genre du site 2");
        site2.setSummary("sommaire du site 2");
        site2.setRelease(2019);
        site2.setMainArtist(mentoss);

        Work site3 = new Work("site3");
        site3.setTitle("titre du site 3");
        site3.setGenre("genre du site 3");
        site3.setSummary("sommaire du site 3");
        site3.setRelease(2020);
        site3.setMainArtist(mentoss);


        Catalogue.getListOfWorks().add(site1);
        Catalogue.getListOfWorks().add(site2);
        Catalogue.getListOfWorks().add(site3);

        for (Work work : Catalogue.getListOfWorks()) {
            System.out.println(work.getTitle()+" ("+work.getRelease()+")");
        }

    }
}
