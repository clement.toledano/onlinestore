package com.directmedia.onlinestore.core.entity;

import java.util.HashSet;
import java.util.Set;

public class ShoppingCart {

    Set<Work> item = new HashSet();

    public Set<Work> getItem() {
        return item;
    }

    public void setItem(Set<Work> item) {
        this.item = item;
    }
}
