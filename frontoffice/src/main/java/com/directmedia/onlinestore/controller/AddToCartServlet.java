package com.directmedia.onlinestore.controller;

import com.directmedia.onlinestore.core.entity.Catalogue;
import com.directmedia.onlinestore.core.entity.ShoppingCart;
import com.directmedia.onlinestore.core.entity.Work;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Optional;
import java.util.Set;

@WebServlet(name = "AddToCartServlet", urlPatterns = {"/addToCart"})
public class AddToCartServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("text/html");

        String idAsString = request.getParameter("identifiant");
        long idAsLong = Long.parseLong(idAsString);

        ShoppingCart cart = (ShoppingCart) request.getSession().getAttribute("cart");

        if (cart == null){
            cart = new ShoppingCart();
            request.getSession().setAttribute("cart", cart);
        }

//        for (Work w : Catalogue.getListOfWorks()) {
//            if (w.getId() == idAsLong) {
//                cart.getItem().add(w);
//                break;
//            }
//        }
        // API STREAM
       Optional<Work> optionalWork = Catalogue.listOfWorks.stream().filter(work -> work.getId()==idAsLong).findAny();
        if (optionalWork.isPresent()){
            cart.getItem().add(optionalWork.get());
        }

        out.println("Oeuvre ajoutée au caddie "+cart.getItem().size());
        out.println("<br>");
        out.println("<br>");
        out.print("<a href=\"catalogue\">Accès au catalogue des oeuvres</a>");

    }


}
