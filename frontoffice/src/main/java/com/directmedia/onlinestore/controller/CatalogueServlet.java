package com.directmedia.onlinestore.controller;


import com.directmedia.onlinestore.core.entity.Artist;
import com.directmedia.onlinestore.core.entity.Catalogue;
import com.directmedia.onlinestore.core.entity.Work;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name="CatalogueServlet",urlPatterns= {"/catalogue"})
public class CatalogueServlet extends HttpServlet {


    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out=response.getWriter();

        if(Catalogue.getListOfWorks().isEmpty()){
            Artist mentoss = new Artist("Mentoss");

            Work site1 = new Work("site1");
            site1.setTitle("titre du site 1");
            site1.setGenre("genre du site 1");
            site1.setSummary("sommaire du site 1");
            site1.setRelease(2017);
            site1.setMainArtist(mentoss);

            Work site2 = new Work("site2");
            site2.setTitle("titre du site 2");
            site2.setGenre("genre du site 2");
            site2.setSummary("sommaire du site 2");
            site2.setRelease(2019);
            site2.setMainArtist(mentoss);

            Work site3 = new Work("site3");
            site3.setTitle("titre du site 3");
            site3.setGenre("genre du site 3");
            site3.setSummary("sommaire du site 3");
            site3.setRelease(2020);
            site3.setMainArtist(mentoss);

            Catalogue.getListOfWorks().add(site1);
            Catalogue.getListOfWorks().add(site2);
            Catalogue.getListOfWorks().add(site3);
        }


            out.print("<h1>FILS DE PUTE !!!!!!!</h1><br/><br/>");

        for (Work work : Catalogue.getListOfWorks()) {
            out.println("#"+work.getId()+" - "+work.getTitle()+" ("+work.getRelease()+") <a href=\"work-details?id=" + work.getId() +"\">details</a><br/>");
        }
    }
}
