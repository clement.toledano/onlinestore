<%--
  Created by IntelliJ IDEA.
  User: mentoss
  Date: 23/02/2020
  Time: 15:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>

<html>
<head>
    <title>Détails de l'oeuvre</title>
</head>
<body>
<h1>Descriptif de l'oeuvre</h1>
<h3>Titre : ${work.title}</h3>
<p>
    Genre : ${work.genre}<br />
    Année de parution : ${work.release}<br />
    Artiste : ${work.mainArtist.name}<br />
    Résumé : ${work.summary}
</p><br />
<form action="addToCart" method="post">
    <input type="hidden" name="identifiant" value="${work.id}" />
    <input type="submit" value="Ajouter au caddie" />
</form>
<a href="catalogue">Retourner au catalogue</a>
</body>
</html>
